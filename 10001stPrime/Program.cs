﻿using System;

namespace _10001stPrime
{
    //    By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

    //What is the 10 001st prime number?
    public class Program
    {
        static void Main(string[] args)
        {

            int number = 3;
            int counter = 1;

            while (counter <= 10001)
            {
                for (int divider = 2; divider < number; divider++)
                {
                    if (number % divider == 0)
                    {
                        break;
                    }
                    if (divider == number - 1)
                    {
                        counter++;
                        if (counter == 10001)
                        {
                            Console.WriteLine(number);
                            break;
                        }
                    }
                }
                number++;
            }

            Console.Read();
        }
    }
}
